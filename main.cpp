#include <QThread>
#include "operate_on_bulb.h"
#include "dialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Dialog w;
    w.show();

    QThread operate_on_bulb_thread;
    operate_on_bulb operate_on_bulb_obj;
    operate_on_bulb_obj.moveToThread(&operate_on_bulb_thread);
    operate_on_bulb_thread.start();

    return a.exec();
}
