#ifndef OPERATE_ON_BULB_H
#define OPERATE_ON_BULB_H

#include <QObject>
#include <QTcpSocket>

#include <QDebug>

class operate_on_bulb : public QObject
{
    Q_OBJECT
public:
    explicit operate_on_bulb(QObject *parent = nullptr);

signals:

public slots:
    void toggle_bulb(QString bulb_ip, quint16 bulb_port);
    void start_cf(QString bulb_ip, quint16 bulb_port);
    void set_ct(QString bulb_ip, quint16 bulb_port);
    void set_bright(QString bulb_ip, quint16 bulb_port, int brightness);


private:
    QTcpSocket *tcp_socket;
    QDataStream *tcp_data_stream;
};

#endif // OPERATE_ON_BULB_H
